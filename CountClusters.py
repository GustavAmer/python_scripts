#from a 2D matrix count horizontally or vertically connected clusters of '1'
def countClusters(rows, column, grid):
    count = 0
    for row in range(rows):
        for cell in range(column):
            if grid[row][cell] == 1:
                count += 1
                findClusters(grid, row, cell, rows, column)
    return count


def findClusters(grid, x, y, maxX, maxY):
    if (x < 0 or y < 0 or x >= maxX or y >= maxY):
        return
    if (grid[x][y] == 1):
        grid[x][y] = 0
        findClusters(grid, x, y + 1, maxX, maxY)
        findClusters(grid, x, y - 1, maxX, maxY)
        findClusters(grid, x + 1, y, maxX, maxY)
        findClusters(grid, x - 1, y, maxX, maxY)