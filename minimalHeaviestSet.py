#find subset of arr where nr of elements is minimal and sum of elements exceeds the sum of the rest
def minimalHeaviestSetA(arr):
    arr.sort()
    arrSum = sum(arr)
    minHeaviestSetASum = 0
    minHeaviestSetA = []
    while minHeaviestSetASum <= arrSum:
        temp = arr.pop()
        minHeaviestSetASum += temp
        arrSum -= temp
        minHeaviestSetA.insert(0, temp)
    return minHeaviestSetA