# a number of log lines are given. Lines consist of id, space and log. Logs are either all numbers or characters. Eg: '123 this is log line' or '123 321 654 889'
# sort lines containing characters based on log alphabetical value. If they are equal use ID. Numeric log lines come at the end in original order.

##imperfect solution
def reorderLines(logFileSize, logLines):
    words = {}
    w = []
    numbers = []
    for line in logLines:
        id, log = line.split(" ", 1)
        if (any(c.isalpha() for c in log)):
            words[id] = log
            w.append(line)
        else:
            numbers.append(line)
    sortedWords = sorted(w, cmp=compare2)
    ret = []
    for line in sortedWords:
        ret.append(line[0] + " " + line[1])
    return ret + numbers

def compare2(item1, item2):
    items1 = item1.split(" ")
    items2 = item2.split(" ")
    for i in range(1, len(items1)):
        if (items1[i] == items2[i]):
            continue
        if (items1[i] < items2[i]):
            return -1
        else:
            return 1
    if items1[0] < items2[0]:
        return -1
    return 1